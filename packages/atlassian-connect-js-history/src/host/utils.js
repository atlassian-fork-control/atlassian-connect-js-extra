import {
  ANCHOR_PREFIX,
  QUERY_KEY_PREFIX,
  QUERY_KEY_DELIMITER,
  STATE_AP_KEY
} from './constants';

export function find (arr, iterator) {
  for (var i = 0, l = arr.length; i < l; i += 1) {
    if (iterator(arr[i])) {
      return arr[i];
    }
  }
}

export function values (obj) {
  var values = [];
  if (!obj) return values;
  for (var key in obj) {
    values.push(obj[key]);
  }
  return values;
}

export function stripHash (str) {
  return str ? str.slice(str.search("#") + 1) : null;
}

export function stripHashPrefix (text) {
  if (text === undefined || text === null || text === "") {
    return "";
  }
  return text.toString().replace(new RegExp("^" + ANCHOR_PREFIX), "");
}

export function addHash (str) {
  return '#' + str;
}

export function addHashPrefix (text) {
  if (text === undefined || text === null) {
    throw "You must supply text to prefix";
  }
  return ANCHOR_PREFIX + stripHashPrefix(text);
}

export function splitQueryParam (paramString) {
  const split = paramString ? paramString.split('=') : [];
  const key = split[0] || null;
  const value = split[1] || null;
  if (split.length === 2) {
    const key = split[0];
    const value = split[1];
    return {
      key,
      value
    }
  } else {
    return null;
  }
}

export function joinQueryParam (paramObject) {
  if (('key' in paramObject) && ('value' in paramObject)) {
    return [paramObject.key, '=', paramObject.value].join('');
  } else {
    return null;
  }
}

export function parseQuery (queryString) {
  // returns an array of objects
  const params = queryString ? queryString.split('&') : [];
  return params.map(function (paramString) {
    return splitQueryParam(paramString);
  }).filter((item) => item);
}

export function formatQuery (queryArr) {
  // returns a query string
  const mapped = queryArr.map(function (paramObject) {
    return joinQueryParam(paramObject);
  }).filter((item) => item);
  return mapped.length ? mapped.join('&') : null;
}

export function stripAddonQueryPrefix (queryObject) {
  const split = queryObject.key ? queryObject.key.split(QUERY_KEY_DELIMITER) : [];
  const prefix = QUERY_KEY_PREFIX;
  if (split.length >= 3) {
    if (prefix === split[0]) {
      // be greedy for addonKey instead of queryKey
      const addonKey = split.slice(1, split.length - 1).join(QUERY_KEY_DELIMITER);
      const queryKey = split.slice(split.length - 1).join(QUERY_KEY_DELIMITER);
      return {
        addonKey: addonKey,
        key: queryKey,
        value: queryObject.value
      };
    } else {
      return queryObject;
    }
  } else {
    return queryObject;
  }
}

export function addAddonQueryPrefix (queryObject) {
  const prefix = QUERY_KEY_PREFIX;
  if ('addonKey' in queryObject && queryObject.addonKey) {
    const queryKey = [prefix, queryObject.addonKey, queryObject.key].join(QUERY_KEY_DELIMITER);
    return {
      key: queryKey,
      value: queryObject.value
    };
  } else {
    return queryObject;
  }
}

export function normalizeQueryByAddons (queryArr) {
  let addons = [];
  queryArr.forEach(function (query) {
    const key = query.key;
    const value = query.value;
    const addonKey = query.addonKey;
    const filtered = addons.filter(function (addon) {
      return addon.key === addonKey;
    });
    if (filtered && filtered.length) {
      const found = filtered[0];
      found.query[key] = value;
    } else {
      addons.push({
        key: addonKey,
        query: {
          [key]: value
        }
      });
    }
  });
  return addons;
}

export function denormalizeQueryByAddons (addonArr) {
  let query = [];
  addonArr.forEach(function (addon) {
    const addonKey = addon.key;
    Object.keys(addon.query).forEach(function (key) {
      const value = addon.query[key];
      query.push({
        addonKey,
        key,
        value
      })
    });
  });
  return query;
}

export function deepCopy (obj) {
  return JSON.parse(JSON.stringify(obj));
}

export function wrapState (state) {
  const rootState = {};
  rootState[STATE_AP_KEY] = state;
  return rootState;
}

export function unwrapState (state) {
  return (state && state[STATE_AP_KEY]) ? state[STATE_AP_KEY] : null;
}

export function createEvent (eventName) {
  if (typeof Event === 'function') { // chrome, firefox, safari
    const evt = new Event(eventName);
    return evt;
  } else {
    const evt = document.createEvent('Event'); // ie11
    evt.initEvent(eventName, false, false);
    return evt;
  }
}

export function callConnectHost (cb) {
  if (require && require.amd) {
    require(['connect-host'], function (connectHost) {
      cb(connectHost);
    });
  } else {
    cb(window.connectHost);
  }
}

export function log(toLog, type){
  if(!type) {
    type = 'log';
  }
  console[type].call(null, 'Atlassian Connect JS History: ', toLog);
}
