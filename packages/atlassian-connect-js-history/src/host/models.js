import url from 'url';
import {
  find,
  deepCopy,
  addHash,
  addHashPrefix,
  stripHash,
  stripHashPrefix,
  parseQuery,
  formatQuery,
  addAddonQueryPrefix,
  stripAddonQueryPrefix,
  normalizeQueryByAddons,
  denormalizeQueryByAddons
} from './utils';

export function Addon (opts) {
  opts = opts || {};
  if (!opts.key) {
    throw new Error('missing key for addon');
  }
  this.key = opts.key;
  this.state = opts.state;
  this.query = opts.query || {};
  return this;
}

export function Addons (opts) {
  opts = opts || {};
  opts.models = opts.models || [];
  if (!(opts.models instanceof Array)) {
    opts.models = [opts.models];
  }
  opts.models = opts.models.map(function (model) {
    if (model instanceof Addon) {
      return model;
    } else {
      return new Addon(model);
    }
  });
  this.models = opts.models;
  return this;
}

Addons.prototype.add = function (addon) {
  this.models.push(addon);
};

Addons.prototype.remove = function (addon) {
  this.models.splice(this.models.indexOf(addon), 1);
};

Addons.prototype.find = function (key) {
  return find(this.models, function (addon) {
    return addon.key === key;
  });
};

Addons.prototype.merge = function (addon) {
  const found = this.find(addon.key);
  if (found) {
    this.remove(found);
  }
  this.add(addon);
};

export function Current (opts) {
  opts = opts || {};
  if (!opts.url) {
    throw new Error('missing url');
  }
  const parsed = url.parse(opts.url);
  const addons = new Addons();
  const queryArr = parseQuery(parsed.query).map(stripAddonQueryPrefix);
  const normalizedQueryAddons = normalizeQueryByAddons(queryArr);
  const queryAddons = normalizedQueryAddons.filter((item) => item.key);
  const queryGlobal = normalizedQueryAddons.filter((item) => !item.key);
  queryAddons.forEach(function (item) {
    const created = new Addon(item);
    addons.add(created);
  });
  this.url = new URL({
    url: opts.url,
    addons: addons,
    global: queryGlobal
  })
  this.state = new State(opts.state);
  return this;
}

export function Change (opts, addonKey) {
  const type = typeof opts;
  const defaults = url.parse(window.location.href);
  const urlObject = {};
  const options = {};
  if (type === 'string') {
    urlObject.protocol = defaults.protocol;
    urlObject.slashes = defaults.slashes;
    urlObject.hostname = defaults.hostname;
    urlObject.pathname = defaults.pathname;
    urlObject.port = defaults.port;
    urlObject.search = defaults.search;
    urlObject.query = defaults.query;
    urlObject.hash = addHash(addHashPrefix(opts));
  } else if (type === 'object') {
    const parsed = opts.href ? url.parse(opts.href) : defaults;
    urlObject.protocol = defaults.protocol;
    urlObject.slashes = defaults.slashes;
    urlObject.hostname = defaults.hostname;
    urlObject.port = defaults.port;
    urlObject.pathname = parsed.pathname || defaults.pathname;
    urlObject.search = null;
    urlObject.query = null;
    urlObject.hash = opts.hash ? addHash(addHashPrefix(opts.hash)) : null;
    options.state = opts.state;
    options.query = opts.query;
  } else {
    throw new Error('invalid option type');
  }
  const addonsState = new Addons();
  const addonsURL = new Addons();
  if (options.state) {
    const addonState = new Addon({
      key: addonKey,
      state: options.state
    });
    addonsState.add(addonState);
  }
  if (options.query) {
    const addonURL = new Addon({
      key: addonKey,
      query: options.query
    });
    addonsURL.add(addonURL);
  }
  const changeState = new State({
    addons: addonsState
  });
  const changeURL = new URL({
    url: url.format(urlObject),
    addons: addonsURL
  });
  this.state = changeState;
  this.url = changeURL;
  return this;
}

export function State (opts) {
  opts = opts || {};
  opts.addons = opts.addons || [];
  if (!(opts.addons instanceof Addons)) {
    opts.addons = new Addons(opts.addons);
  }
  this.addons = opts.addons;
  return this;
}

State.prototype.merge = function (instance) {
  const self = this;
  instance.addons.models.forEach(function (addon) {
    self.addons.merge(addon);
  });
};

State.prototype.render = function () {
  return deepCopy(this);
};

export function URL (opts) {
  opts = opts || {};
  opts.addons = opts.addons || [];
  opts.global = opts.global || [];
  if (!(opts.parsed instanceof url.Url)) {
    opts.parsed = url.parse(opts.url);
  }
  if (!(opts.addons instanceof Addons)) {
    opts.addons = new Addons(opts.addons);
  }
  this.parsed = opts.parsed;
  this.addons = opts.addons;
  this.global = opts.global; // global query parameters that are not associated with addons
  this.title = opts.title || window.document.title;
}

URL.prototype.isURLEqual = function (instance) {
  return this.render().url === instance.render().url;
};

URL.prototype.isPathnameEqual = function (instance) {
  const protocolEqual = this.parsed.protocol === instance.parsed.protocol;
  const slashesEqual = this.parsed.slashes === instance.parsed.slashes;
  const hostnameEqual = this.parsed.hostname === instance.parsed.hostname;
  const pathnameEqual = this.parsed.pathname === instance.parsed.pathname;
  return (protocolEqual && slashesEqual && hostnameEqual && pathnameEqual);
};

URL.prototype.merge = function (instance) {
  const self = this;
  self.parsed.hash = instance.parsed.hash;
  instance.addons.models.forEach(function (addon) {
    self.addons.merge(addon);
  });
};

URL.prototype.render = function () {
  const deep = deepCopy(this);
  const urlObj = deep.parsed;
  const addonsDenormalizedQueryAddons = denormalizeQueryByAddons(this.addons.models);
  const globalDenormalizedQueryAddons = denormalizeQueryByAddons(this.global);
  const allQueryParams = addonsDenormalizedQueryAddons.concat(globalDenormalizedQueryAddons);
  const prefixedQueryArr = allQueryParams.map(addAddonQueryPrefix);
  const formattedQuery = formatQuery(prefixedQueryArr);
  urlObj.query = formattedQuery;
  urlObj.search = urlObj.query ? ('?' + urlObj.query) : null;
  deep.url = url.format(urlObj);
  return deep;
};

export function Route (opts) {
  opts = opts || {};
  if (!opts.url) {
    throw new Error('missing url option');
  }
  if (!(opts.url instanceof URL)) {
    opts.url = new URL(opts.url);
  }
  if (!opts.state) {
    throw new Error('missing state option');
  }
  if (!(opts.state instanceof State)) {
    opts.state = new State(opts.state);
  }
  this.url = opts.url;
  this.state = opts.state;
}

Route.prototype.merge = function (route) {
  this.url.merge(route.url);
  this.state.merge(route.state);
};

Route.prototype.render = function (addonKey) {
  return deepCopy({
    key: addonKey,
    hash: stripHashPrefix(stripHash(this.url.parsed.hash)),
    query: this.url.addons.find(addonKey) ? this.url.addons.find(addonKey).query : null,
    title: this.url.title,
    href: this.url.parsed.href,
    state: this.state.addons.find(addonKey) ? this.state.addons.find(addonKey).state : null
  });
};

