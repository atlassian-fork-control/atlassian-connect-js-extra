let fullPageIframe = require("../elements/fullPageIframe");
let iframeCommands = {
    switchToIframe: function(){
        let selector = fullPageIframe.selector;
        var page = this.api;
        page.waitForElementVisible(selector);
        return new Promise(function(resolve, reject){    
            page.getAttribute(selector, "id", (frameIdAttribute) => {
                let iframeId = frameIdAttribute.value;
                page.frame(frameIdAttribute.value, () => {
                    resolve(iframeId);
                });
            });
        });
    }
};

module.exports = {
  url: function(){
    if(process.env.PRODUCT === 'confluence') {
      return this.api.launchUrl + '/wiki/plugins/servlet/ac/connect-js-integration-test-app/confluence-general-page';
    } else {
      return this.api.launchUrl + '/plugins/servlet/ac/connect-js-integration-test-app/jira-general-page';
    }
  },
  elements: {
    fullPageIframe: fullPageIframe
  },
  commands: [iframeCommands]
};