const GET_LOCATION_BTN = "@getLocationBtn";
const GET_LOCATION_DEBUG ="@getLocationDebug";
const GO_BTN = "@goBtn";
const GO_URL = "/some-addon-key/some-module-key";
const RELOAD_DEBUG = "@reloadTestInput";
const RELOAD_ADDON_BTN = "@reloadAddon";

function getModuleKey(){
    return (process.env.PRODUCT === 'confluence') ? 'confluence-general-page' : 'jira-general-page';
}

module.exports = {
    beforeEach: function(browser){
        var page = browser.page.navigatorGeneralPage();
        page.navigate();
    },
    'AP.navigator.getLocation' : function (browser) {
        let page = browser.page.navigatorGeneralPage();
        // method only supported in confluence
        if(process.env.PRODUCT === 'jira') {
            return;
        }
        browser.page.navigatorGeneralPage().switchToIframe().then((iframeId) => {
            browser.page.navigatorAddonIframe().click(GET_LOCATION_BTN);
            browser.page.navigatorAddonIframe().waitForElementPresent(GET_LOCATION_DEBUG)
            .expect.element(GET_LOCATION_DEBUG).text.to.equal(JSON.stringify({
                target: "addonmodule",
                context: {
                    addonKey: "connect-js-integration-test-app",
                    moduleKey: getModuleKey(),
                    context: {}
                }
            }));
        });
    },
    'AP.navigator.go': function(browser) {
        browser.page.navigatorGeneralPage().switchToIframe().then((iframeId) => {
            browser.page.navigatorAddonIframe().click(GO_BTN);
            browser.assert.urlContains(GO_URL);
        });
    },
    'AP.navigator.reload': function(browser) {
        browser.page.navigatorGeneralPage().switchToIframe().then((iframeId) => {
            const iframePage = browser.page.navigatorAddonIframe();
            iframePage.waitForElementPresent(RELOAD_DEBUG).setValue(RELOAD_DEBUG, 'debugValue');
            iframePage.expect.element(RELOAD_DEBUG).to.have.value.that.equals('debugValue');
            iframePage.click(RELOAD_ADDON_BTN);
            browser.page.navigatorGeneralPage().switchToIframe().then((iframeId) => {
                browser.page.navigatorAddonIframe().expect.element(RELOAD_DEBUG).to.have.value.that.equals('');
            });
        });
    }
};