var _ = require('lodash');
var babel = require('gulp-babel');
var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var derequire = require('gulp-derequire');
var envify = require('envify/custom');
var source = require('vinyl-source-stream');
var unreachableBranch = require('unreachable-branch-transform');
var watch = require('gulp-watch');
var watchify = require('watchify');



function build(entryModule, distModule, options) {
    var bundler = watchify(
        browserify(entryModule, {
            debug: false,
            standalone: distModule,
            sourceMap: false
        })
        .transform(babelify.configure({sourceMap:false}))
        .transform(envify(options.env || {}))
        .transform(unreachableBranch)
    );

    function rebundle() {
        return bundler.bundle()
            .on('error', function (err) {
                gutil.log(gutil.colors.red('Browserify error'), err.message);
                this.emit('end');
            })
            .pipe(source(distModule + '.js'))
            .pipe(buffer())
            .pipe(derequire())
            .pipe(gulp.dest('./dist'));
    }

    if (options.watch) {
        bundler.on('update', function () {
            gutil.log('Rebundling', gutil.colors.blue(entryModule));
            rebundle(bundler, options);
        });
    }

    gutil.log('Bundling', gutil.colors.blue(entryModule));
    return rebundle(bundler, options);
}

function buildPlugin(options) {
    options = options || {};
    return gulp.src('./src/plugin/index.js')
        .pipe(concat('plugin.js'))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(babel())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist'));
}

function buildHost(options) {
    options = options || {};
    return build('./src/host/index.js', 'connect-host-cookie', {
        env: {ENV: 'host'},
        watch: options.watch
    });
}

gulp.task('host:build', buildHost);
gulp.task('host:watch', buildHost.bind(null, {watch: true}));

gulp.task('watch', ['host:watch']);
gulp.task('build', ['host:build']);

gulp.task('default', ['build']);